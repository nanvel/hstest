from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', 'hstest.apps.notes.views.notes', name='notes'),
    url(r'^admin/', include(admin.site.urls)),
)
